<?php
// src/AppBundle/Controller/LocaleSwitchController.php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class LocaleSwitchController extends Controller {

    /**
     * @Route("/en", name="switch_language_english")
     */
    public function switchLanguageEnglishAction() {
        return $this->switchLanguage('en');
    }

    /**
     * @Route("/de", name="switch_language_german")
     */ 
    public function switchLanguageGermanAction() {
        return $this->switchLanguage('de');
    }

    private function switchLanguage($locale) {
        $this->get('session')->set('_locale', $locale);
        return $this->redirect($this->generateUrl('app_homepage'));
    }

}
